package ch.pinnatec.open.ssoclient;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {
	@GetMapping(path="/public")
	public String getPublicHtml() {
		return "<h4>public message</h4>";
	}
	
	@GetMapping(path="/secure")
	public String getSecureHtml() {
		return "<h4>secure message</h4>";
	}

	@GetMapping(path="/admin")
	public String getAdminHtml() {
		return "<h4>admin message</h4>";
	}
}
