package ch.pinnatec.open.ssoclient;

import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigResolver {
  //separate keycloakConfigResolver to solve cyclic dependencies keycloak 15.0.2 and spring 2.6

  @Bean
  public KeycloakSpringBootConfigResolver KeycloakConfigResolver() {
    return new KeycloakSpringBootConfigResolver();
  }
}
